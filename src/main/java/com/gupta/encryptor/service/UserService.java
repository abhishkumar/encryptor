package com.gupta.encryptor.service;

import com.gupta.encryptor.modal.User;

public interface UserService {

    public User addNewUser(User user);

    public User getUserByEmailAndPassword(String email,String password);
    public boolean validateEmailId(String email);
    public boolean validateUserName(String userName);

}
