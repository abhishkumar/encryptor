package com.gupta.encryptor.service;

import com.gupta.encryptor.modal.EncryptedFile;
import com.gupta.encryptor.modal.FileToBeDeleted;
import com.gupta.encryptor.repository.FilesRemoveRepository;
import com.gupta.encryptor.utility.Constants;
import com.gupta.encryptor.utility.Utility;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class FileServiceImpl implements FileService {

    private static final Logger log = LogManager.getLogger(FileServiceImpl.class);

    @Autowired
    private FilesRemoveRepository repository;
    @Autowired
    private Environment environment;
    @Override
    public void removeDecryptedFile(FileToBeDeleted file){repository.save(file);}

    @Override
    public List<EncryptedFile> fetchAllEncryptedFiles(Integer userId){
        String filePath = environment.getProperty("file.store.path")+userId;
        List<EncryptedFile> list = null;
        try{
            list = new ArrayList<>();
            File[] filesList = new File(String.valueOf(Paths.get(filePath))).listFiles();
            List<EncryptedFile> finalList = list;
            Arrays.stream(filesList).filter(obj-> !obj.getName().contains("_decrypted")).forEach(obj -> {
                FileTime creationTime = null;
                try {
                    creationTime = (FileTime) Files.getAttribute(Paths.get(obj.getAbsolutePath()), "creationTime");
                } catch (IOException e) {
                    log.error("Unable to get file creation time {} due to {}",obj.getName(),Utility.getStacktrace(e));
                }
                String time = creationTime.toString();
                EncryptedFile tmp = new EncryptedFile(fetchFileTypeIconPath(obj.getName()),obj.getName(),time.substring(0,time.lastIndexOf(".")).replace("T"," "));
                finalList.add(tmp);
            });
            log.info("Total files found {}",list.size());
        }catch (Exception e){
            log.error("Error while retrieving files from user directory {}", Utility.getStacktrace(e));
        }
        return list;
    }

    @Override
    public void removeFile(String fileName){
        try {
            Files.deleteIfExists(Paths.get(fileName));
        } catch (IOException e) {
            log.error("Unable to remove file {} due to {}",fileName,Utility.getStacktrace(e));
        }
    }

    private String fetchFileTypeIconPath(String fileName){
        return Constants.FileTypes.TYPES.get(fileName.substring(fileName.lastIndexOf(".")+1));
    }
}
