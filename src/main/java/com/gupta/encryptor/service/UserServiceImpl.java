package com.gupta.encryptor.service;

import com.gupta.encryptor.modal.User;
import com.gupta.encryptor.dao.UserDao;
import com.gupta.encryptor.utility.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao dao;

    @Override
    public User addNewUser(User user) {
        user.setPassword(Utility.encryptPassword(user.getPassword()));
        return dao.saveUser(user);
    }
    @Override
    public User getUserByEmailAndPassword(String email, String password) {
        return dao.getUserByEmailPassword(email,Utility.encryptPassword(password));
    }

    @Override
    public boolean validateEmailId(String email) {
        return dao.checkUserByEmail(email);
    }

    @Override
    public boolean validateUserName(String userName) {
        return dao.checkUserByUserName(userName);
    }

}
