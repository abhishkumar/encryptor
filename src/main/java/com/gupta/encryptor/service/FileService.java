package com.gupta.encryptor.service;

import com.gupta.encryptor.modal.EncryptedFile;
import com.gupta.encryptor.modal.FileToBeDeleted;

import java.util.List;

public interface FileService {

    public void removeDecryptedFile(FileToBeDeleted file);

    public List<EncryptedFile> fetchAllEncryptedFiles(Integer userId);
    public void removeFile(String fileName);
}
