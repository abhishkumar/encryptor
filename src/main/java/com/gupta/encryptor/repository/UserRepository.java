package com.gupta.encryptor.repository;

import com.gupta.encryptor.modal.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User,Integer> {

    public User findTopByOrderByUserIdDesc();
    public User findByEmailAndPassword(String email, String password);
    public User findByEmail(String email);
    public User findByUserName(String userName);
}
