package com.gupta.encryptor.repository;

import com.gupta.encryptor.modal.FileToBeDeleted;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface FilesRemoveRepository extends MongoRepository<FileToBeDeleted,Integer> {
}
