package com.gupta.encryptor.dao;

import com.gupta.encryptor.modal.User;

public interface UserDao {

    public User saveUser(User user);

    public User fetchUserById(Integer userId);

    public User getUserByEmailPassword(String email, String password);
    public boolean checkUserByEmail(String email);
    public boolean checkUserByUserName(String userName);
}
