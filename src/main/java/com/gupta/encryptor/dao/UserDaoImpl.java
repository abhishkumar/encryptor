package com.gupta.encryptor.dao;

import com.gupta.encryptor.modal.User;
import com.gupta.encryptor.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserDaoImpl implements UserDao {

    @Autowired
    private UserRepository repository;
    @Override
    public User saveUser(User user) {
        Integer newId = 1;
        try{
            newId = repository.findTopByOrderByUserIdDesc().getUserId();
        }catch (Exception e) {
            e.printStackTrace();
            newId = 1;
        }
        user = new User(newId+1,user.getUserName(),user.getEmail(),user.getAge(),user.getPassword());
        return repository.save(user);
    }

    @Override
    public User fetchUserById(Integer userId) {
        return repository.findById(userId).get();
    }

    @Override
    public User getUserByEmailPassword(String email, String password) {
        return repository.findByEmailAndPassword(email,password);
    }


    public boolean checkUserByEmail(String email) {
        return repository.findByEmail(email) != null;
    }

    public boolean checkUserByUserName(String userName) {
        return repository.findByUserName(userName) != null;
    }

}
