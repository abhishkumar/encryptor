package com.gupta.encryptor.modal;

import org.springframework.data.mongodb.core.mapping.Document;

@Document("filestodelete")
public class FileToBeDeleted {

    private Integer fileId;
    private String fileName;

    public Integer getFileId() {
        return fileId;
    }

    public void setFileId(Integer fileId) {
        this.fileId = fileId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public FileToBeDeleted(Integer fileId, String fileName) {
        this.fileId = fileId;
        this.fileName = fileName;
    }

    public FileToBeDeleted() {
        super();
    }
}
