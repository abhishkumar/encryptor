package com.gupta.encryptor.modal;

public class EncryptedFile {

    private String type;
    private String fileName;
    private String encryptionDate;

    public EncryptedFile() {
        super();
    }

    public EncryptedFile(String type, String fileName, String encryptionDate) {
        this.type = type;
        this.fileName = fileName;
        this.encryptionDate = encryptionDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getEncryptionDate() {
        return encryptionDate;
    }

    public void setEncryptionDate(String encryptionDate) {
        this.encryptionDate = encryptionDate;
    }
}
