package com.gupta.encryptor.utility;

import com.gupta.encryptor.modal.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSession;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;

/**
 * This class contains the utility methods that
 * requires in the application.
 * This call contains all static methods.
 */
@Component
public class Utility {

    private static final Logger log = LogManager.getLogger(Utility.class);
    //This variable is used to encrypt the string with MD5 encryption.
    private static final char[] hexArray = "0123#456789AB$%CDEF".toCharArray();
    @Autowired
    private Environment env;

    private static final String PATH_SEPERATOR = "/";

    /**
     * This method is used to encrypt user password
     * with MD5 encryption logic to it can't be decrypted
     * @param pass password that is going to be encrypted
     * @return Encrypted form of password
     */
    public static String encryptPassword(String pass) {

        String passwd="";
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(pass.getBytes());
            byte[] encodedPassword = md.digest();
            passwd = bytesToHex(encodedPassword);

        } catch(NoSuchAlgorithmException nsae){
            log.error("No such Algorithm Exception {}",getStacktrace(nsae));
        }
        catch (Exception e) {
            log.error("Exception while encrypting password {}",getStacktrace(e));
        }
        return passwd;
    }

    /**
     * This method is used internally to convert bytes to HexForm
     * @param bytes to convert hex
     * @return converted hex String
     */
    private static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }


    public static boolean isUserAvailable(HttpSession session){
        boolean flag = false;
        User user = (User) session.getAttribute(Constants.SessionAttributes.USER);
        if(user != null && user.getUserId() != null && user.getUserId() >0){
            flag = true;
        }
        return flag;
    }

    /**
     * This method check for the user directory existence and
     * create a full fileName with path.
     * @param userId for directory_name
     * @param fileName uploaded file name
     * @return fileName with path
     */
    public static String newFileName(Integer userId,String fileName,String filePath){
        String fullPath = filePath;
        try{
            Path path = Paths.get(fullPath+userId);
            if(Files.exists(path) && Files.isDirectory(path)){
                log.info("User Folder already exists ");
            }else{
                Files.createDirectory(path);
            }
            fullPath = fullPath+userId+PATH_SEPERATOR+fileName;
            if(Files.exists(Paths.get(fullPath))){
                LocalDateTime currentDate = LocalDateTime.now();
                String appendTime = String.valueOf(currentDate.getYear()+currentDate.getMonthValue()+currentDate.getDayOfMonth()+currentDate.getHour()+ currentDate.getMinute());
                fullPath = fullPath.replace(".",appendTime+".");
            }
        }catch (Exception e) {
            log.error("Error while creating folder to store the file ...{}",getStacktrace(e));
        }

        return fullPath;
    }


    public static String getStacktrace(Exception e) {
        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw));
        return sw.toString();
    }


}
