package com.gupta.encryptor.utility;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * This class is responsible to encrypt and decrypt
 * different file types.
 */
@Component
public class FileEncrypterDecrypter {

    private static final Logger log = LogManager.getLogger(FileEncrypterDecrypter.class);
    private final String PADDING_TYPE = "AES/CBC/PKCS5PADDING";
    private final String ALGORITHM = "AES";
    private final String SC_1 = "�";

    /**
     * This method encrypts all Doc & Docx file content and store them into a
     * specified location.
     * @param file Multipart file for encryption
     * @param fileName by which encrypted file is going to be stored
     * @param key the encryption key
     * @throws Exception Any exceptions
     */
    public void encryptDoc(MultipartFile file, String fileName,String key) throws Exception {
        SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(), ALGORITHM);
        Cipher cipher = Cipher.getInstance(PADDING_TYPE);
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec,new IvParameterSpec(key.getBytes()));
        byte[] iv = cipher.getIV();
        try (FileOutputStream fileOut = new FileOutputStream(fileName);
             CipherOutputStream cipherOut = new CipherOutputStream(fileOut, cipher)) {
            fileOut.write(iv);
            cipherOut.write(extractContentFromWord(file).getBytes());
        }
    }

    /**
     * This method decrypts all Doc & Docx file content and store them into a
     * specified location.
     * @param file Multipart file for decryption
     * @param fileName by which decrypted file is going to be stored
     * @param key the encryption key
     * @throws Exception
     */
    public void decryptDoc(MultipartFile file,String fileName,String key) throws Exception {
        SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(), ALGORITHM);
        Cipher cipher = Cipher.getInstance(PADDING_TYPE);
        cipher.init(Cipher.DECRYPT_MODE, skeySpec,new IvParameterSpec(key.getBytes()));
        byte [] bytes = cipher.doFinal(file.getBytes());
        fileName = fileName.replace(".doc","_decrypted.doc");
        FileOutputStream out = new FileOutputStream(fileName);
        XWPFDocument document = new XWPFDocument();
        String content = new String(bytes);
        if(content.contains(SC_1)){
            content = content.substring(content.lastIndexOf(SC_1)+1);

        }
        document.createParagraph().createRun().setText(content);
        document.write(out);
        out.write(bytes);
        out.close();
    }

    public void encryptTxt(MultipartFile file, String fileName,String key) throws Exception {
        SecretKeySpec skeySpec = new SecretKeySpec(convertKeyIntoByteArray(key), ALGORITHM);
        Cipher cipher = Cipher.getInstance(PADDING_TYPE);
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec,new IvParameterSpec(convertKeyIntoByteArray(key)));
        byte [] bytes = cipher.doFinal(file.getBytes());
        FileOutputStream out = new FileOutputStream(new File(fileName));
        out.write(bytes);
        out.close();
    }

    public void decryptTxt(MultipartFile file,String fileName,String key) throws Exception {
        SecretKeySpec skeySpec = new SecretKeySpec(convertKeyIntoByteArray(key), ALGORITHM);
        Cipher cipher = Cipher.getInstance(PADDING_TYPE);
        cipher.init(Cipher.DECRYPT_MODE, skeySpec,new IvParameterSpec(convertKeyIntoByteArray(key)));
        byte [] bytes = cipher.doFinal(file.getBytes());
        fileName = fileName.replace(".txt","_decrypted.txt");
        FileOutputStream out = new FileOutputStream(new File(fileName));
        out.write(bytes);
        out.close();
    }

    private byte[] convertKeyIntoByteArray(String key){
        byte[] raw = key.getBytes(StandardCharsets.UTF_8);
        if (raw.length != 16) {
            throw new IllegalArgumentException("Invalid key size.");
        }
        return raw;
    }

    public void fileTypeEncryptor(String ext,MultipartFile file,String fileName,String key) throws Exception {
        switch(ext){
            case "doc":
            case "docx":
                encryptDoc(file,fileName,key);
                break;
            case "txt":
                encryptTxt(file,fileName,key);
                break;
            case "pdf":
                encryptDoc(file,fileName,key);
                break;
        }
    }

    public void fileTypeDecryptor(String ext,MultipartFile file,String fileName,String key) throws Exception {
        switch(ext){
            case "doc":
            case "docx":
                decryptDoc(file,fileName,key);
                break;
            case "txt":
                decryptTxt(file,fileName,key);
                break;
            case "pdf":
                decryptDoc(file,fileName,key);
                break;
        }
    }


    public String extractContentFromWord(MultipartFile file){
        String text = "";
        try{
            XWPFDocument doc = new XWPFDocument(file.getInputStream());
            XWPFWordExtractor extractor = new XWPFWordExtractor(doc);
            text = extractor.getText();
            extractor.close();
            doc.close();
        }catch (Exception e){
            log.error("Error while extracting text content from a word file. Due to {}",Utility.getStacktrace(e));
            log.error(e.getStackTrace());
        }
        return text;
    }

    public void decryptLocalFile(String fileName,String ext,String key) throws Exception {
        switch (ext){
            case "doc" : case "docx":
                decryptLocalDoc(fileName,key);
                break;
            case "txt" :
                decryptLocalTxt(fileName,key);
                break;
        }
    }


    public void decryptLocalDoc(String fileName,String key) throws Exception {
        SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(), ALGORITHM);
        Cipher cipher = Cipher.getInstance(PADDING_TYPE);
        cipher.init(Cipher.DECRYPT_MODE, skeySpec,new IvParameterSpec(key.getBytes()));
        File file = new File(fileName);
        byte [] bytes = cipher.doFinal(Files.readAllBytes(file.toPath()));
        fileName = fileName.replace(".doc","_decrypted.doc");
        FileOutputStream out = new FileOutputStream(fileName);
        XWPFDocument document = new XWPFDocument();
        String content = new String(bytes);
        if(content.contains(SC_1)){
            content = content.substring(content.lastIndexOf(SC_1)+1);
        }
        document.createParagraph().createRun().setText(content);
        document.write(out);
        out.write(bytes);
        out.close();
    }

    public void decryptLocalTxt(String fileName,String key) throws Exception{
        SecretKeySpec skeySpec = new SecretKeySpec(convertKeyIntoByteArray(key), ALGORITHM);
        Cipher cipher = Cipher.getInstance(PADDING_TYPE);
        cipher.init(Cipher.DECRYPT_MODE, skeySpec,new IvParameterSpec(convertKeyIntoByteArray(key)));
        File file = new File(fileName);
        byte [] bytes = cipher.doFinal(Files.readAllBytes(file.toPath()));
        fileName = fileName.replace(".txt","_decrypted.txt");
        FileOutputStream out = new FileOutputStream(new File(fileName));
        out.write(bytes);
        out.close();
    }
}
