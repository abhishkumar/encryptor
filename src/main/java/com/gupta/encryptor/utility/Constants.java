package com.gupta.encryptor.utility;

import java.util.HashMap;
import java.util.Map;

public interface Constants {


    public static interface Views {
        public static final String HOME_PAGE = "index";
        public static final String LOGIN = "signin";
        public static final String ERROR = "error";
        public static final String DASHBOARD = "dashboard";
        public static final String FILES = "files";
    }

    public static interface Actions {
        public static final String ROOT = "/";
        public static final String LOGIN = "/login";
        public static final String VALIDATE_EMAIL_USER_NAME = "/validateUserNameEmail";
    }

    public static interface SessionAttributes {
        public static final String USER = "user";
    }

    public static interface FileTypes {
        public static final Map<String, String> TYPES = new HashMap<String, String>() {{
            put("doc", "/img/doc.png");
            put("docx", "/img/doc.png");
            put("txt", "/img/txt.png");
        }};
    }
}
