package com.gupta.encryptor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class EncryptorApplication {

	public static void main(String[] args) {
		SpringApplication.run(EncryptorApplication.class, args);
	}

}
