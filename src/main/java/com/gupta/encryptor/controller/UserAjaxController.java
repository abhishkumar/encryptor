package com.gupta.encryptor.controller;

import com.gupta.encryptor.modal.User;
import com.gupta.encryptor.service.FileService;
import com.gupta.encryptor.utility.Utility;
import com.google.gson.JsonObject;
import com.gupta.encryptor.service.UserService;
import com.gupta.encryptor.utility.Constants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/user/ajax")
public class UserAjaxController {

    private static final Logger log = LogManager.getLogger(UserAjaxController.class);
    @Autowired
    private UserService service;
    @Autowired
    private FileService fileService;
    @Value("${file.store.path}")
    private String filePath;
    @GetMapping(value = Constants.Actions.VALIDATE_EMAIL_USER_NAME)
    public String checkMobileAndUserName(@RequestParam String value,@RequestParam String type){
        JsonObject response = new JsonObject();
        boolean flag = false;
        log.info("Checking user Email/Mobile existence for duplicate ");
        try{
            if(type.equalsIgnoreCase("email")){
                flag = service.validateEmailId(value);
            }else{
                flag = service.validateUserName(value);
            }
            response.addProperty("data", flag);
        }catch (Exception e) {
            log.error("Error while validating Email/mobile due to {}",e.getMessage());
            log.error(Utility.getStacktrace(e));
        }
        return response.toString();
    }


    @DeleteMapping("/removeFile")
    public String removeEncryptedFile(@RequestParam String fileName, HttpSession session) {
        JsonObject response = new JsonObject();
        try {
            User user = (User) session.getAttribute(Constants.SessionAttributes.USER);
            fileName =filePath+user.getUserId()+"/"+fileName;
            fileService.removeFile(fileName);
            response.addProperty("data", Boolean.TRUE);
        }catch (Exception e){
            log.error("Error while removing encrypted file from the system due to {}",e.getMessage());
            log.error(Utility.getStacktrace(e));
        }
        return response.toString();
    }

}
