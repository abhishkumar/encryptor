package com.gupta.encryptor.controller;

import com.gupta.encryptor.modal.User;
import com.gupta.encryptor.service.UserService;
import com.gupta.encryptor.utility.Constants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;

@Controller
public class UserController {

    private static final Logger log = LogManager.getLogger(UserController.class);

    @Autowired
    private UserService userService;
    @PostMapping("/newUser")
    public String registerNewUser(@ModelAttribute User user, Model model, HttpSession session){
        String view = "";
        try{
            log.info("Registering new user ...");
            User obj = userService.addNewUser(user);
            if(obj.getUserId() != null){
                log.info("User successfully registered. Storing into session");
                session.setAttribute(Constants.SessionAttributes.USER,obj);
                view = Constants.Views.DASHBOARD;
            }else{
                log.warn("Some error occurred while saving user...");
                model.addAttribute("msg","Unable to add User Please try again.");
                view = Constants.Views.HOME_PAGE;
            }
        }catch (Exception e){
            log.error("Error while storing user into database due to "+e.getMessage());
            log.error(e.getStackTrace());
            view = Constants.Views.ERROR;
        }
        return view;
    }

    @PostMapping(value = Constants.Actions.LOGIN)
    public String login(@RequestParam String email, @RequestParam String password,HttpSession session,Model model) {
        String view = "";
        try{
            log.info("Checking user by email & mobile ...");
            User user = userService.getUserByEmailAndPassword(email,password);
            if(user == null){
                log.info("Invalid credentials ");
                model.addAttribute("msg","Invalid credentials");
                view = Constants.Views.LOGIN;
            }else{
                log.info("User found & storing into session ");
                session.setAttribute(Constants.SessionAttributes.USER,user);
                view = Constants.Views.DASHBOARD;
            }
        }catch (Exception e) {
            log.error("Error while login due to "+e.getMessage());
            log.error(e.getStackTrace());
            view = Constants.Views.ERROR;
        }
        return view;
    }


    @GetMapping("/logout")
    public String logout(HttpSession session){
        log.info("Removing user from session & invalidating session ...");
        session.removeAttribute(Constants.SessionAttributes.USER);
        session.invalidate();
        return Constants.Views.LOGIN;
    }
}
