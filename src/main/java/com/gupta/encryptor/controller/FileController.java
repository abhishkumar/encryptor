package com.gupta.encryptor.controller;

import com.gupta.encryptor.modal.User;
import com.gupta.encryptor.utility.Constants;
import com.gupta.encryptor.modal.FileToBeDeleted;
import com.gupta.encryptor.service.FileService;
import com.gupta.encryptor.utility.FileEncrypterDecrypter;
import com.gupta.encryptor.utility.Utility;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;


@RestController
public class FileController {

    private static final Logger log = LogManager.getLogger(FileController.class);
    @Autowired
    private FileEncrypterDecrypter fileUtility;
    @Autowired
    private Environment environment;
    @Autowired
    private FileService fileService;

    @PostMapping(value = "/encryptFile",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<byte[]> encrypt(@RequestPart("file") MultipartFile file, @RequestParam String key, HttpSession session) throws IOException {
        InputStream in = null;
        try{
            String fileName = file.getOriginalFilename();
            log.info("Uploaded file name for encryption is : {}",fileName);
            User user = (User) session.getAttribute(Constants.SessionAttributes.USER);
            String ext =  fileName.substring(fileName.lastIndexOf('.') + 1);
            String newFileName = Utility.newFileName(user.getUserId(), fileName,environment.getProperty("file.store.path"));
            log.info("Calling encryptor start...");
            fileUtility.fileTypeEncryptor(ext,file,newFileName,key);
            log.info("Calling encryptor end...");
            in = new FileInputStream(newFileName);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            headers.setContentDisposition(ContentDisposition.attachment().filename(fileName).build());
            return new ResponseEntity<>(IOUtils.toByteArray(in), headers, HttpStatus.OK);
        }catch (Exception e){
            log.error("Error while encrypting the file due to {} ",e.getMessage());
            log.debug(e.getStackTrace());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PostMapping(value = "/decryptFile",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<byte[]> decrypt(@RequestPart("file") MultipartFile file,@RequestParam String key,HttpSession session) throws IOException {
        InputStream in = null;
        try{
            String fileName = file.getOriginalFilename();
            log.info("Uploaded file name for decryption is : {}",fileName);
            User user = (User) session.getAttribute(Constants.SessionAttributes.USER);
            String ext =  fileName.substring(fileName.lastIndexOf('.') + 1);
            String newFileName = Utility.newFileName(user.getUserId(), fileName,environment.getProperty("file.store.path"));
            log.info("Calling decryptor start...");
            fileUtility.fileTypeDecryptor(ext,file,newFileName,key);
            log.info("Calling decryptor end...");
            String fullName = newFileName;
            if(!ext.equalsIgnoreCase("txt")){
                fullName = fullName.replace(".doc","_decrypted.doc");
            }else{
                fullName = fullName.replace(".txt","_decrypted.txt");
            }
            in = new FileInputStream(fullName);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            headers.setContentDisposition(ContentDisposition.attachment().filename(fileName).build());
            fileService.removeDecryptedFile(new FileToBeDeleted(1,fullName));
            return new ResponseEntity<>(IOUtils.toByteArray(in), headers, HttpStatus.OK);
        }catch (Exception e){
            log.error("Error while decrypting the file due to {} ",e.getMessage());
            log.debug(e.getStackTrace());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/download",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<byte[]> downloadFile(@RequestParam String fileName,@RequestParam String key, HttpSession session){
        InputStream in = null;
        try{
            User user = (User) session.getAttribute(Constants.SessionAttributes.USER);
            String ext =  fileName.substring(fileName.lastIndexOf('.') + 1);
            String fullName = environment.getProperty("file.store.path")+user.getUserId()+"/"+fileName;
            fileUtility.decryptLocalFile(fullName,ext,key);
            if(!ext.equalsIgnoreCase("txt")){
                fullName = fullName.replace(".doc","_decrypted.doc");
            }else{
                fullName = fullName.replace(".txt","_decrypted.txt");
            }
            in = new FileInputStream(fullName);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            headers.setContentDisposition(ContentDisposition.attachment().filename(fileName).build());
            fileService.removeDecryptedFile(new FileToBeDeleted(1,fullName));
            return new ResponseEntity<>(IOUtils.toByteArray(in), headers, HttpStatus.OK);
        }catch (Exception e){
            log.error("Error while downloading decrypted file {} due to {}",fileName,Utility.getStacktrace(e));
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }


    @GetMapping("/remove")
    public String deleteFile(@RequestParam String fileName, HttpSession session, Model model) {
        try{
            User user = (User) session.getAttribute(Constants.SessionAttributes.USER);
            fileService.removeFile(environment.getProperty("file.store.path")+user.getUserId()+"/"+fileName);
            model.addAttribute("files",fileService.fetchAllEncryptedFiles(user.getUserId()));
        }catch (Exception e){
            log.error("Error while removing file for system {}",Utility.getStacktrace(e));
        }

        return Constants.Views.FILES;
    }


}
