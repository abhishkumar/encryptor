package com.gupta.encryptor.controller;

import com.gupta.encryptor.modal.User;
import com.gupta.encryptor.utility.Constants;
import com.gupta.encryptor.utility.Utility;
import com.gupta.encryptor.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpSession;

@Controller
public class MainController {

    @Autowired
    private FileService fileService;

    @GetMapping(value = Constants.Actions.ROOT)
    public String home(){return Constants.Views.HOME_PAGE;}

    @GetMapping("/signin")
    public String navSignIn(){return "signin";}

    @GetMapping("/files")
    public String navFiles(HttpSession session, Model model){
        String viewName = "";
        if(Utility.isUserAvailable(session)){
            viewName = Constants.Views.FILES;
            User user = (User) session.getAttribute(Constants.SessionAttributes.USER);
            model.addAttribute("files",fileService.fetchAllEncryptedFiles(user.getUserId()));
        }else{
            viewName = Constants.Views.LOGIN;
        }
        return viewName;
    }

    @GetMapping("/dashboard")
    public String navDashboard(HttpSession session){
        String viewName = "";
        if(Utility.isUserAvailable(session)){
            viewName = Constants.Views.DASHBOARD;
        }else{
            viewName = Constants.Views.LOGIN;
        }
        return viewName;
    }
}
