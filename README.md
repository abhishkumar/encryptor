# encryptor

## Name
Files Encryptor/Decryptor

## Description

This project can be used as utility project.
In this project you can encrypt/decrypt the the Document and txt files.
Encrypted files are stored over the server but no one can decrypt/read
it without the encryption key that has been used at the time of encryption.

## Installation

This proejct is created using spring boot.
So you can simply take the war file and deploy in any webserver.


## Usage
This project can be used globally to share the encrypted files.
Where no one can view the file without the encryption key.

## Support
I am still upgrading this project time to time.
So till we havn't get our goal i am going to support.

## Roadmap
As per current target we are going to add the support for every file type.

## Project status
This project is in still under development.
